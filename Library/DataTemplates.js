/**
 * Registration templates
 */
export const authTemplate = {
    login: 3,
    email: '@',
    password: 6
}

/**
 * Tickets templates
 */
export const minTicketCost = 5

/**
 * Lottery numbers
 */
export const lotteryNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

/**
 * Draw interval
 */
export const drawInterval = '*/5 * * * *'

/**
 * Prize multiplier
 */
export const prizeMultiplier = [0, 1.1, 1.5, 2.5, 5, 10]
