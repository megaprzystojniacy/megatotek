/**
 * Database Error
 */
export class databaseError extends Error {
    constructor(errorExtraParams) {
        super('Wystąpił błąd')
        this.type = 1
        console.log({ error: errorExtraParams.errorName, cause: errorExtraParams.errorMessage })
    }
}

/**
 * Auth errors
 */
export const badDataError = { result: 'Wprowadzono błędne dane', type: 1 }
export const userExistsError = { result: 'Użytkownik już istnieje', type: 1 }
export const userLoginError = { result: 'Błędny login lub hasło', type: 1 }

/**
 * Ticket errors
 */
export const lowAccountBalance = { result: 'Posiadasz zbyt mało środków na koncie', type: 1 }
export const tooLowDeposit = { result: 'Zbyt niska kwota depozytu (min. 5)', type: 1 }

/**
 * Wallet errors
 */
export const fundsValidationError = { result: 'Wprowadzono błędną liczbę', type: 1 }
