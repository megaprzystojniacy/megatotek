/**
 * Auth callbacks
 */
export const userRegistrationCallback = { result: 'Zarejestrowano pomyślnie', type: 0 }
export const userLoginCallback = { result: 'Zalogowano pomyślnie', type: 0 }
export const logoutCallback = { result: 'Pomyślnie wylogowano', type: 0 }

/**
 * Lottery callbacks
 */
export const newTicketCallback = { result: 'Pomyślnie zakupiono los', type: 0 }

/**
 * Wallet callbaks
 */
export const addFundsCallback = { result: 'Pomyślnie doładowano środki', type: 0}
export const withdrawFundsCallback = { result: 'Pomyślnie wypłacono środki', type: 0}
