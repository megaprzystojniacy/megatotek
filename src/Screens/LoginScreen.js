import LoginForm from '../components/Forms/LoginForm.js'
import Webname from '../components/WebName/Webname.js'
import { FormScreen, FormScreenWrapper } from './AppStyle.js'

const LoginScreen = ({ setLogIn, setRegister, translate }) => {
    let minus = translate - 2 * translate
    return (
        <FormScreen translate={minus}>
            <FormScreenWrapper onClick={() => setLogIn(false)} />
            <Webname />
            <LoginForm setLogIn={setLogIn} setRegister={setRegister} />
        </FormScreen>
    )
}

export default LoginScreen
