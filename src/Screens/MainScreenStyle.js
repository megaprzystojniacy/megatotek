import styled from 'styled-components'

export const SideBanner = styled.div`
    width: 25vw;
    height: 100vh;
    background-color: #1e001d;
    box-shadow: 0 0 40px black;
    min-height: 100vh;
    justify-content: center;
    align-items: center;
    overflow-y: auto;

    @media (max-width: 900px) {
        width: 100vw;
    }
`

export const MiddleWrapper = styled.div`
    width: 50vw;
    height: 100vh;
    display: flex;
    flex-flow: column nowrap;
    justify-content: flex-start;
    align-items: center;
    overflow-x: hidden;
    overflow-y: auto;
    box-sizing: border-box;

    @media (max-width: 900px) {
        width: 100vw;
    }
`

export const ChangeScreen = styled.button`
    background: none;
    color: #bbb;
    border: none;
    outline: none;
    padding: 0.5em;
    font-size: 1rem;
    display: none;
    cursor: pointer;

    @media (max-width: 900px) {
        display: block;
    }
`

export const NavigationWrapper = styled.div`
    width: 100vw;
    display: flex;
    position: fixed;
    justify-content: space-between;
`
