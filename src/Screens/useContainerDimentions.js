import { useEffect, useState } from 'react'

export const useContainerDimensions = (myRef) => {
    const getDimensions = () => ({
        scaleX: myRef.current.offsetWidth / 450,
        scaleY:
            myRef.current.offsetWidth / 700 > myRef.current.offsetHeight / 700
                ? 1000
                : myRef.current.offsetHeight / 700
    })

    const [dimensions, setDimensions] = useState({ width: 0, height: 0 })

    useEffect(() => {
        const handleResize = () => {
            setDimensions(getDimensions())
        }

        if (myRef.current) {
            setDimensions(getDimensions())
        }

        window.addEventListener('resize', handleResize)

        return () => {
            window.removeEventListener('resize', handleResize)
        }
    }, [myRef])

    return dimensions
}
