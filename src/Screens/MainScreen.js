import { AppBodyRow } from './AppStyle.js'
import Webname from '../components/WebName/Webname.js'
import LastDraw from '../components/LastDraw/LastDraw.js'
import BuyTicket from '../components/BuyTicket/BuyTicket.js'
import { ChangeScreen, MiddleWrapper, NavigationWrapper, SideBanner } from './MainScreenStyle.js'
import { useEffect, useRef, useState } from 'react'
import { useContainerDimensions } from './useContainerDimentions.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faListUl, faArrowRight, faArrowLeft, faUser } from '@fortawesome/free-solid-svg-icons'
import SmallTicket from '../components/TicketHistory/SmallTicket.js'
import TicketList from '../components/TicketHistory/TicketList.js'
import { getWinningTickets, getUserData } from '../fetchFunctions.js'
import LoginScreen from './LoginScreen.js'
import RegisterScreen from './RegisterScreen.js'
import UserInfo from '../components/UserInfo/UserInfo.js'

const MainScreen = () => {
    const [userData, setUserData] = useState()
    const [unusedUserTickets, setUnusedUserTickets] = useState([])
    const [usedUserTickets, setUsedUserTickets] = useState([])
    const [numberOfUserTickets, setNumberOfUserTickets] = useState('')
    const [ticketLimit, setTicketLimit] = useState(5)

    const userHandler = (addLimit = false) => {
        if (addLimit) {
            getUserData(
                ticketLimit + 5,
                setUserData,
                setUnusedUserTickets,
                setUsedUserTickets,
                setNumberOfUserTickets
            )
            setTicketLimit(ticketLimit + 5)
        } else {
            getUserData(
                ticketLimit,
                setUserData,
                setUnusedUserTickets,
                setUsedUserTickets,
                setNumberOfUserTickets
            )
        }
    }

    const [logIn, setLogIn] = useState(false)
    const [register, setRegister] = useState(false)

    useEffect(() => {
        userHandler()
    }, [logIn])

    const [winningTickets, setWinningTickets] = useState([{ n1: 0, n2: 0, n3: 0, n4: 0, n5: 0, n6: 0 }])
    const [winningTicket, setWinningTicket] = useState({ n1: 0, n2: 0, n3: 0, n4: 0, n5: 0, n6: 0 })

    const winningTicketsHandler = () => {
        getWinningTickets(setWinningTickets, setWinningTicket)
    }

    useEffect(() => {
        winningTicketsHandler()
    }, [])

    const [translate, setTranslate] = useState(-100)

    const ScrollHistory = () => {
        setTranslate(0)
    }

    const ScrollMain = () => {
        setTranslate(-100)
    }

    const ScrollUser = () => {
        setTranslate(-200)
    }

    const ScrollScreen = [ScrollHistory, ScrollMain, ScrollUser]
    const [currentScreen, setCurrentScreen] = useState(1)

    const MiddleWrapperRef = useRef(null)

    const { scaleX, scaleY } = useContainerDimensions(MiddleWrapperRef)

    const [scale, setScale] = useState(1)

    useEffect(() => {
        scaleX < scaleY ? setScale(scaleX) : setScale(scaleY)
    }, [scaleX, scaleY])

    const [touchStart, setTouchStart] = useState(0)
    const [touchEnd, setTouchEnd] = useState(0)

    const handleTouchStart = (e) => {
        setTouchStart(e.targetTouches[0].clientX)
    }

    const handleTouchMove = (e) => {
        setTouchEnd(e.targetTouches[0].clientX)
    }

    const handleTouchEnd = () => {
        if (touchStart - touchEnd > 150) {
            try {
                ScrollScreen[currentScreen + 1]()
                setCurrentScreen(currentScreen + 1)
            } catch {}
        }

        if (touchStart - touchEnd < -150) {
            try {
                ScrollScreen[currentScreen - 1]()
                setCurrentScreen(currentScreen - 1)
            } catch {}
        }
    }

    return (
        <AppBodyRow
            translate={translate}
            onTouchStart={(e) => handleTouchStart(e)}
            onTouchMove={(e) => handleTouchMove(e)}
            onTouchEnd={(e) => handleTouchEnd(e)}
        >
            {logIn && <LoginScreen setLogIn={setLogIn} setRegister={setRegister} translate={translate} />}
            {register && (
                <RegisterScreen setLogIn={setLogIn} setRegister={setRegister} translate={translate} />
            )}
            <SideBanner>
                <NavigationWrapper>
                    <div />
                    <ChangeScreen onClick={ScrollMain}>
                        Powrót <FontAwesomeIcon icon={faArrowRight} />
                    </ChangeScreen>
                </NavigationWrapper>
                <h4> Historia losów </h4>
                <SmallTicket ticket={winningTicket} />
                <TicketList tickets={winningTickets} />
            </SideBanner>
            <MiddleWrapper ref={MiddleWrapperRef} scale={scale}>
                <NavigationWrapper>
                    <ChangeScreen onClick={ScrollHistory}>
                        <FontAwesomeIcon icon={faListUl} /> Najnowsze wygrane
                    </ChangeScreen>
                    <div />
                    <ChangeScreen onClick={ScrollUser}>
                        Mój Profil <FontAwesomeIcon icon={faUser} />
                    </ChangeScreen>
                </NavigationWrapper>
                <Webname />
                <LastDraw ticket={winningTicket} scale={scale} />
                <BuyTicket scale={scale} setLogIn={setLogIn} userHandler={userHandler} />
            </MiddleWrapper>
            <SideBanner>
                <NavigationWrapper>
                    <ChangeScreen onClick={ScrollMain}>
                        <FontAwesomeIcon icon={faArrowLeft} /> Powrót{' '}
                    </ChangeScreen>
                    <div />
                </NavigationWrapper>
                <UserInfo
                    userData={userData}
                    setUserData={setUserData}
                    unusedUserTickets={unusedUserTickets}
                    usedUserTickets={usedUserTickets}
                    setLogIn={setLogIn}
                    numberOfUserTickets={numberOfUserTickets}
                    setTicketLimit={setTicketLimit}
                    ticketLimit={ticketLimit}
                    userHandler={userHandler}
                />
            </SideBanner>
        </AppBodyRow>
    )
}

export default MainScreen
