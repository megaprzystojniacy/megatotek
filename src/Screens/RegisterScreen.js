import Webname from '../components/WebName/Webname.js'
import RegisterForm from '../components/Forms/RegisterForm.js'
import { FormScreen, FormScreenWrapper } from './AppStyle.js'

const RegisterScreen = ({ setLogIn, setRegister, translate }) => {
    let minus = translate - 2 * translate
    return (
        <FormScreen translate={minus}>
            <FormScreenWrapper onClick={() => setLogIn(false)} />
            <Webname />
            <RegisterForm setLogIn={setLogIn} setRegister={setRegister} />
        </FormScreen>
    )
}

export default RegisterScreen
