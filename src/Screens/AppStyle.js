import styled from 'styled-components'

export const AppBodyRow = styled.div`
    position: fixed;
    width: 100vw;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: row nowrap;
    color: white;
    transition: transform 0.3s;
    box-sizing: border-box;

    @media (max-width: 900px) {
        width: 300vw;
        transform: translateX(${(props) => props.translate}vw);
    }
`

export const AppBodyColumn = styled.div`
    width: 100%;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap;
    color: white;
    padding: 1em;
    box-sizing: border-box;
`

export const FormScreen = styled.div`
    position: fixed;
    width: 100vw;
    height: 100vh;
    background-color: rgba(1, 1, 1, 0.5);
    backdrop-filter: blur(10px);
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap;
    color: white;
    padding: 1em;
    box-sizing: border-box;
    z-index: 10;
    @media (max-width: 900px) {
        left: ${(props) => props.translate}vw;
    }
`

export const FormScreenWrapper = styled.div`
    position: absolute;
    height: 100%;
    width: 100%;
`
