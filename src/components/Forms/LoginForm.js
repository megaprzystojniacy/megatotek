import React, { useEffect, useState } from 'react'
import { ColoredBorder, ColoredBorderSecondLayer } from '../commonStyles.js'
import ColoredForm from './ColoredForm.js'
import { FormButton, FormLink, FormWrapper, InputWrapper, Error } from './ColoredFormStyle.js'
import { color1, color2 } from '../../assets/colors.js'
import { logIn } from '../../fetchFunctions.js'

const LoginForm = ({setRegister, setLogIn}) => {
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const loginHandler = () => {
        logIn(login, password, setErrorMessage)
    }

    const changeScreen = () => {
        setLogIn(false)
        setRegister(true)
    }

    useEffect(() => {
        errorMessage.type===0 && setLogIn(false)
    }, [errorMessage])

    return (
        <FormWrapper>
        <ColoredBorderSecondLayer color={color2}>
            <ColoredBorder color={color1}>
                
                <Error>{errorMessage.result}</Error>
                <InputWrapper color={color1}>
                    <ColoredForm
                        placeholder="login"
                        name="login"
                        type="text"
                        value={login}
                        setValue={setLogin}
                    />
                </InputWrapper>
                <InputWrapper color={color2}>
                    <ColoredForm
                        placeholder="hasło"
                        name="password"
                        type="password"
                        value={password}
                        setValue={setPassword}
                    />
                </InputWrapper>
                <FormButton color={color1} onClick={loginHandler}>
                    Zaloguj
                </FormButton>
                <FormLink color={color2} onClick={changeScreen}>
                    Zarejestruj się
                </FormLink>
                
            </ColoredBorder>
        </ColoredBorderSecondLayer>
        </FormWrapper>
    )
}

export default LoginForm
