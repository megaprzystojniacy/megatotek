import React from 'react'
import { ColoredInput, InputUnderline } from './ColoredFormStyle.js'

const ColoredForm = ({ type, name, placeholder, value, setValue }) => {
    const valueHandler = (event) => {
        setValue(event.target.value)
    }

    return (
        <>
            <ColoredInput
                type={type}
                placeholder={placeholder}
                name={name}
                value={value}
                onChange={valueHandler}
            />
            <InputUnderline id="hover" />
        </>
    )
}

export default ColoredForm
