import React, { useEffect, useState } from 'react'
import { ColoredBorder, ColoredBorderSecondLayer } from '../commonStyles.js'
import ColoredForm from './ColoredForm.js'
import { FormButton, FormLink, FormWrapper, InputWrapper, Error } from './ColoredFormStyle.js'
import { register } from '../../fetchFunctions.js'
import { color1, color2 } from '../../assets/colors.js'

const RegisterForm = ({setRegister, setLogIn}) => {
    const [login, setLogin] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const registerHandler = () => {
        register(login, email, password, setErrorMessage)
    }

    const changeScreen = () => {
        setRegister(false)
        setLogIn(true) 
    }

    useEffect(() => {
        errorMessage.type === 0 && changeScreen()
    }, [errorMessage])

    return (
        <FormWrapper>
        <ColoredBorderSecondLayer color={color2}>
            <ColoredBorder color={color1}>
                <Error>{errorMessage.result}</Error>
                <InputWrapper color={color2}>
                    <ColoredForm
                        placeholder="login"
                        name="login"
                        type="text"
                        value={login}
                        setValue={setLogin}
                    />
                </InputWrapper>
                <InputWrapper color={color1}>
                    <ColoredForm
                        placeholder="e-mail"
                        name="email"
                        type="email"
                        value={email}
                        setValue={setEmail}
                    />
                </InputWrapper>
                <InputWrapper color={color2}>
                    <ColoredForm
                        placeholder="hasło"
                        name="password"
                        type="password"
                        value={password}
                        setValue={setPassword}
                    />
                </InputWrapper>
                <FormButton onClick={registerHandler}>Zarejestruj się</FormButton>
                <FormLink color={color2} onClick={changeScreen}>
                    Zaloguj się
                </FormLink>
            </ColoredBorder>
        </ColoredBorderSecondLayer>
        </FormWrapper>
    )
}

export default RegisterForm
