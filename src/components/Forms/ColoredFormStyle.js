import styled from 'styled-components'
import { color1 } from '../../assets/colors.js'

export const ColoredInput = styled.input`
    border: none;
    outline: none;
    background-color: transparent;
    font-size: inherit;
    padding: 0.5em 1.5em;
    width: 100%;
    box-sizing: border-box;
    color: white;
    transition: 1s;
    text-align: inherit;

    -moz-appearance: textfield;

    &::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
`

export const InputWrapper = styled.div`
    width: 80%;
    margin: 1rem;
    font-size: 1.5rem;

    &:hover #hover {
        box-shadow: 0 0 9px 1px ${props=>props.color};
        transition: 0.2s;
    }
`

export const InputUnderline = styled.div`
    margin-top: -1px;
    box-shadow: 0 0 9px 1px transparent;
    transition: 0.2s;
    height: 2px;
    width: 100%;
    background-color: white;
`

export const FormButton = styled.button`
    border: 3px solid white;
    transition: 0.2s;
    margin: 1rem;
    padding: 1rem 2rem;
    border-radius: 1rem;
    cursor: pointer;
    background: none;
    color: white;
    font-size: 1.5rem;
    font-family: tahoma;

    &:hover {
    box-shadow: inset 0 0 9px ${color1},  0 0 9px ${color1};
    text-shadow: 0 0 9px ${color1};
    transition: 0.2s;
    }
`

export const FormLink = styled.div`
    margin: 0;
    padding: 0;
    font-family: tahoma;
    
    & > a {
        font-size: 1.2rem;
        margin-bottom: 1rem;
        text-decoration: underline;
        cursor: pointer;
        color: white;
    }
    

    &:hover {
        text-shadow: 0 0 9px ${props=>props.color};
        transition: 0.2s;
    }
`

export const FormWrapper = styled.div`
    position: relative;
    z-index: 11;
`

export const Error = styled.div`
    font-family: tahoma;
`