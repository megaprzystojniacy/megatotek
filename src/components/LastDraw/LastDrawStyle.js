import styled from 'styled-components'

export const Wrapper = styled.div`
    transform: scale(${(props) => (props.scale ? (props) => props.scale / 1.3 : 1)});
    margin-top: ${(props) => (props.scale ? (props) => props.scale * 1.4 : 1)}em;
`

export const NumberCircle = styled.div`
    padding: 0.7rem;
    border: 3px solid white;
    border-radius: 50%;
    font-size: 2rem;
    width: 2rem;
    height: 2rem;
    box-shadow: inset 0 0 9px ${(props) => props.color}, 0 0 9px ${(props) => props.color};
`

export const CirclesWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
    padding-top: 1rem;
`

export const Title = styled.h3`
    background-color: #111;
    box-shadow: 0 0 5px 5px #111;
    padding: 0.5rem;
    text-shadow: 0 0 9px ${(props) => props.color};
    position: relative;
    margin: 0;
    margin-top: -3rem;
`
