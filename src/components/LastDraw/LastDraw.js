import { color1, color2, color3 } from '../../assets/colors.js'
import { ColoredBorder, ColoredBorderSecondLayer } from '../commonStyles.js'
import { CirclesWrapper, NumberCircle, Title, Wrapper } from './LastDrawStyle.js'

const LastDraw = ({ ticket, scale }) => {
    return (
        <Wrapper scale={scale}>
            <ColoredBorderSecondLayer color={color2}>
                <ColoredBorder color={color3}>
                    <Title color={color2}>Ostatnie losowanie: </Title>
                    <CirclesWrapper>
                        <NumberCircle color={color1}>{ticket.n1}</NumberCircle>
                        <NumberCircle color={color2}>{ticket.n2}</NumberCircle>
                        <NumberCircle color={color1}>{ticket.n3}</NumberCircle>
                        <NumberCircle color={color2}>{ticket.n4}</NumberCircle>
                        <NumberCircle color={color1}>{ticket.n5}</NumberCircle>
                        <NumberCircle color={color2}>{ticket.n6}</NumberCircle>
                    </CirclesWrapper>
                </ColoredBorder>
            </ColoredBorderSecondLayer>
        </Wrapper>
    )
}

export default LastDraw
