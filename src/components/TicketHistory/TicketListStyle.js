import styled from 'styled-components'

export const List = styled.ul`
    text-align: left;
    list-style-type: none;

    & li {
        display: flex;
        flex-flow: row wrap;
        align-items: center;
        gap: 0.5rem;
        margin-bottom: 0.5em;
    }
`

export const Numbers = styled.div`
    border: 2px solid white;
    font-size: 1rem;
    padding: 0.5em;
    border-radius: 0.5em;
    box-shadow: inset 0 0 9px ${(props) => props.color}, 0 0 9px ${(props) => props.color};
`

export const Date = styled.p`
    font-size: 1rem;
`
