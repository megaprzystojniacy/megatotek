import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  margin: 1em 0;
  width: 100%;
`

export const Numbers = styled.h4`
  writing-mode: vertical-lr;
  transform: rotate(180deg);
  text-align: center;
  border: 2px solid white;
  padding: 0.5em;
  margin: 0 0.5em;
  border-radius: 10px;
  box-shadow: inset 0 0 9px ${props => props.color},  0 0 9px ${props => props.color};
`

export const Right = styled.div`
  font-family: Tahoma, sans-serif;
  font-size: 1rem;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  border: 2px solid white;
  border-radius: 10px;
  padding: 0.5em;
  margin-right: 0.5rem;
  box-shadow: inset 0 0 9px ${props => props.color},  0 0 9px ${props => props.color};
  width: 100%;
  
  & * {
    margin: 0;
  }
`

export const Prize = styled.h4`
  font-size: 1.5em;
`