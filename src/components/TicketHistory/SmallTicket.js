import { Numbers, Prize, Right, Wrapper } from './SmallTicketStyle.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoins } from '@fortawesome/free-solid-svg-icons'
import { color1, color2 } from '../../assets/colors.js'

const SmallTicket = ({ ticket }) => {
    return (
        <Wrapper>
            <Numbers color={color2}>
                {ticket?.n1} {ticket?.n2} {ticket?.n3} {ticket?.n4} {ticket?.n5} {ticket?.n6}
            </Numbers>
            <Right color={color1}>
                <Prize>
                    Wygrana {ticket?.highestPrize ? ticket.highestPrize.toFixed(2) : '0'}{' '}
                    <FontAwesomeIcon icon={faCoins} />
                </Prize>
                <div>
                    {new Date(ticket.createdAt).toLocaleTimeString('pl', {
                        day: '2-digit',
                        month: '2-digit',
                        hour: '2-digit',
                        minute: '2-digit'
                    })}
                </div>
                <p>{ticket?.user?.login ? ticket?.user?.login : 'BRAK ZWYCIĘZCY'}</p>
            </Right>
        </Wrapper>
    )
}

export default SmallTicket
