import { List, Numbers } from './TicketListStyle.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoins } from '@fortawesome/free-solid-svg-icons'
import { color2, color1 } from '../../assets/colors.js'

const TicketList = ({ tickets }) => {
    return (
        <List>
            {tickets.map((ticket, id) => {
                let color = id % 2 === 1 ? color2 : color1
                return (
                    <li key={id}>
                        <Numbers color={color}>
                            {ticket?.n1} {ticket?.n2} {ticket?.n3} {ticket?.n4} {ticket?.n5} {ticket?.n6}
                        </Numbers>
                        {ticket?.highestPrize ? ticket.highestPrize.toFixed(2) : ticket?.deposit || '0'}
                        <FontAwesomeIcon icon={faCoins} />
                        <div>
                            {new Date(ticket.createdAt).toLocaleTimeString('pl', {
                                hour: '2-digit',
                                minute: '2-digit'
                            })}
                        </div>
                    </li>
                )
            })}
        </List>
    )
}

export default TicketList
