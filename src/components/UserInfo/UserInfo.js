import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useState, useEffect } from 'react'
import { Icon, LoginButton, Username, Wrapper, Text, PlusWrapper, Select, Option } from './UserInfoStyle.js'
import { faUserCircle, faCoins, faAdd } from '@fortawesome/free-solid-svg-icons'
import Tickets from './Tickets.js'
import { addFunds, logout } from '../../fetchFunctions.js'
import { color1, color2 } from '../../assets/colors.js'

const UserInfo = ({
    userData,
    unusedUserTickets,
    usedUserTickets,
    setUserData,
    setLogIn,
    numberOfUserTickets,
    ticketLimit,
    userHandler
}) => {
    const [visibleSelect, setVisibleSelect] = useState(false)

    const showingSelect = () => {
        visibleSelect ? setVisibleSelect(false) : setVisibleSelect(true)
    }

    const handleAdding = (amount) => {
        addFunds(amount, userHandler)

        setVisibleSelect(false)
    }

    if (userData) {
        return (
            <Wrapper>
                <Icon>
                    <FontAwesomeIcon icon={faUserCircle} />
                </Icon>
                <Username>
                    {userData.login} {userData.balance} <FontAwesomeIcon icon={faCoins} />{' '}
                    <PlusWrapper>
                        <FontAwesomeIcon icon={faAdd} onClick={showingSelect} />
                        {visibleSelect && (
                            <Select>
                                <Option onClick={() => handleAdding(10)}>10</Option>
                                <Option onClick={() => handleAdding(50)}>50</Option>
                                <Option onClick={() => handleAdding(100)}>100</Option>
                                <Option onClick={() => handleAdding(500)}>500</Option>
                            </Select>
                        )}
                    </PlusWrapper>
                </Username>
                <LoginButton
                    onClick={() => {
                        logout()
                        setUserData()
                    }}
                >
                    Wyloguj
                </LoginButton>
                <Text> Moje Bilety </Text>
                {unusedUserTickets.length === 0 ? (
                    <Text>Zakup kupon, aby wziąć udział w następnym losowaniu!</Text>
                ) : (
                    <Tickets tickets={unusedUserTickets} color={color2} />
                )}

                <Text> Zużyte bilety </Text>
                <Tickets tickets={usedUserTickets} color={color1} />
                {numberOfUserTickets >= ticketLimit - 5 && (
                    <LoginButton
                        onClick={() => {
                            userHandler(true)
                        }}
                    >
                        Pokaż więcej
                    </LoginButton>
                )}
            </Wrapper>
        )
    } else {
        return (
            <>
                <LoginButton onClick={() => setLogIn(true)}>Zaloguj</LoginButton>
                <FontAwesomeIcon icon={faUserCircle} />
            </>
        )
    }
}

export default UserInfo
