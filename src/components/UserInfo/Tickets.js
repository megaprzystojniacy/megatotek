import { Deposit, TicketWrapper } from './UserInfoStyle.js'

const Tickets = ({ tickets, color }) => {
    return (
        <div>
            {tickets.map((ticket, key) => (
                <TicketWrapper color={color} key={key}>
                    <div>
                        {ticket.n1} {ticket.n2} {ticket.n3} {ticket.n4} {ticket.n5} {ticket.n6}
                    </div>
                    <Deposit>Depozyt {ticket.deposit}</Deposit>
                    <Deposit>
                        {new Date(ticket.createdAt).toLocaleTimeString('pl', {
                            hour: '2-digit',
                            minute: '2-digit'
                        })}
                    </Deposit>
                </TicketWrapper>
            ))}
        </div>
    )
}

export default Tickets
