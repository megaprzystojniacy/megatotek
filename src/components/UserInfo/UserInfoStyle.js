import styled from 'styled-components'
import { color1 } from '../../assets/colors.js'

export const LoginButton = styled.button`
    background: #230023;
    box-shadow: 0 0 5px black;
    outline: none;
    border: none;
    color: white;
    font-size: 1.2rem;
    margin: 0.5em;
    transition: 0.2s;
    cursor: pointer;
    padding: 0.5em;
    border-radius: 0.5em;

    &:hover {
        text-shadow: 0 0 5px ${color1};
    }
`

export const Username = styled.div`
    margin: 0.5em;
    display: flex;
    gap: 0.5em;
    justify-content: center;
`

export const Wrapper = styled.div`
    display: flex;
    flex-flow: column nowrap;
    text-align: center;
    margin-top: 1rem;

    & ul li {
        justify-content: center;
    }
`

export const Icon = styled.div`
    & svg {
        height: 4em;
    }
`

export const Text = styled.div`
    font-family: tahoma;
    font-size: 1rem;
    margin-top: 2rem;
    font-weight: 700;
`

export const Deposit = styled.div`
    font-family: tahoma;
    font-size: 1.1rem;
`

export const TicketWrapper = styled.div`
    border: 3px solid white;
    box-shadow: inset 0 0 9px ${(props) => props.color}, 0 0 9px ${(props) => props.color};
    margin: 1rem;
    padding: 1rem;
    border-radius: 1rem;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap;
`

export const PlusWrapper = styled.div`
    cursor: pointer;
`

export const Select = styled.div`
    background-color: #eee;
    border-radius: 0.5em;
    position: absolute;
`

export const Option = styled.div`
    padding: 0.5em 1em;
    font-size: 1rem;
    color: black;
    border-radius: 0.5em;
    transition: 0.5s;

    &:hover {
        background-color: white;
    }
`
