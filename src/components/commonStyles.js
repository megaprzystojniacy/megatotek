import styled from 'styled-components'

export const ColoredBorder = styled.div`
    border: 3px solid white;
    box-shadow: inset 0 0 9px ${props => props.color},  0 0 9px ${props => props.color};
    margin: 1rem;
    padding: 1rem;
    border-radius: 1rem;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap;
    width: 100%;
    max-width: 500px;
`

export const ColoredBorderSecondLayer = styled.div`
    box-shadow: inset 0 0 9px ${props => props.color},  0 0 9px ${props => props.color};
    border: 3px solid white;
    max-width: 500px;
    border-radius: 1rem;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap
`

