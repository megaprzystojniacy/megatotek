import styled from 'styled-components'
import { color1 as color, color2 } from '../../assets/colors.js'
import image from '../../assets/message.png'

export const ComponentWrapper = styled.div`
    position: relative;
    padding: 1em;
    box-sizing: border-box;
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    transform: scale(${(props) => props.scale / 1.1});
    margin-top: ${(props) => props.scale * 3}em;
    margin-bottom: ${(props) => props.scale * 3}em;
`

export const Roof = styled.div`
    width: 150px;
    min-height: 50px;
    border: 3px solid white;
    border-bottom: none;
    box-sizing: border-box;
    border-top-left-radius: 50px;
    border-top-right-radius: 50px;
    box-shadow: inset 0 9px 9px -9px ${color}, inset 9px 0 9px -9px ${color}, inset -9px 0 9px -9px ${color},
        0 0 9px ${color};
    display: flex;
    align-items: flex-end;
    font-size: 0.7rem;
    font-family: 'tahoma';
    padding: 10px 15px;
`

export const Back = styled.div`
    width: 306px;
    height: 80px;

    z-index: 4;
    display: flex;
    justify-content: center;
`

export const BackNeon = styled.div`
    border: 3px solid white;
    border-bottom: 2px solid white;
    border-top-left-radius: 25px;
    border-top-right-radius: 25px;
    box-shadow: inset 0 0 9px ${color}, 0 0 9px ${color};
    width: 100%;
    z-index: 5;
`

export const LeftSide = styled.div`
    width: 0;
    height: 50px;
    border-left: 3px solid white;
    transform: skew(-40deg, 0);
    margin-right: 345px;
    box-shadow: 0 0 9px 2px ${color};
    z-index: 3;
`

export const RightSide = styled.div`
    position: absolute;
    width: 0;
    height: 50px;
    border-right: 3px solid white;
    transform: skew(40deg, 0);
    margin-left: 345px;
    margin-top: 80px;
    box-shadow: 0 0 9px 2px ${color};
    z-index: 3;
`

export const Front = styled.div`
    width: 385px;
    height: 80px;
    border: 3px solid white;
    border-radius: 4px;
    margin-top: -2px;
    box-shadow: 0 0 9px ${color}, inset 0 0 9px ${color};
    z-index: 4;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const Hinge = styled.div`
    position: absolute;
    width: 10px;
    height: 20px;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    border: 3px solid white;
    border-left: none;
    box-shadow: 0 0 9px ${color}, inset 0 0 9px ${color};
    margin-top: 30px;
    margin-left: 315px;
    z-index: 1;
`

export const Lever = styled.div`
    position: absolute;
    border: 2px solid #111;
    border-right: 3px solid white;
    border-bottom: 3px solid white;
    border-bottom-right-radius: 5px;
    width: 25px;
    height: 40px;
    z-index: 0;
    margin-left: 349px;
    margin-top: 5px;
    box-sizing: border-box;
    box-shadow: -10px -10px 4px 5px #111, 0 0 9px ${color}, inset 0 -12px 9px -12px ${color},
        inset -12px 0 9px -12px ${color};
`

export const LeverHead = styled.button`
    position: absolute;
    background-color: #111;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    border: 3px solid white;
    box-shadow: 0 0 9px ${color}, inset 0 0 9px ${color};
    margin-left: 372px;
    margin-top: -15px;
    cursor: pointer;
    z-index: 1;
    transition: 0.5s;

    &:hover {
        transform: scale(1.1);
    }
`

export const Screen = styled.div`
    position: absolute;
    width: 250px;
    height: 40px;
    border: 3px solid white;
    box-shadow: 0 0 9px ${color2}, inset 0 0 9px ${color2};
    border-radius: 20%;
    margin-top: 16px;
    padding-left: 3px;
    z-index: 10;

    display: flex;
    justify-content: space-between;
`

export const ScreenCell = styled.div`
    width: 16%;
    height: 100%;
    border-right: ${(props) => (props.noBorder ? 'none' : '3px solid white')};
    box-sizing: border-box;
`

export const ScreenInput = styled.input`
    color: white;
    background-color: transparent;
    outline: none;
    width: 80%;
    height: 100%;
    border: none;
    font-size: 100%;
    text-align: center;
    font-family: 'neonFont';

    -moz-appearance: textfield;

    &::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
`

export const Message = styled.div`
    position: absolute;
    width: 280px;
    height: 100px;
    background-image: url(${image});
    background-size: contain;
    background-repeat: no-repeat;
    margin-top: 57px;
`

export const BuyButton = styled.button`
    width: 50%;
    height: 70%;
    border: 3px solid white;
    transition: 0.2s;
    margin: 1rem;
    padding: 0 2rem;
    border-radius: 1rem;
    cursor: pointer;
    background: none;
    color: white;
    font-size: 1.2rem;

    &:hover {
        box-shadow: inset 0 0 9px ${(props) => props.color}, 0 0 9px ${(props) => props.color};
        text-shadow: 0 0 9px ${(props) => props.color};
        transition: 0.2s;
    }
`

export const DepositInputWrapper = styled.div`
    font-size: 1rem;
    padding: 0.5em;
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    text-align: right;
    width: 6rem;

    &:hover #hover {
        box-shadow: 0 0 9px 1px ${(props) => props.color};
        transition: 0.2s;
    }

    & div {
        text-align: center;
    }
`

export const IconWrapper = styled.div`
    width: 0;
    transform: translate(-27px, 7px) scale(0.65);
`
