import { useState } from 'react'
import {
    Back,
    ComponentWrapper,
    Front,
    LeftSide,
    RightSide,
    Roof,
    Hinge,
    LeverHead,
    Lever,
    Screen,
    ScreenCell,
    ScreenInput,
    Message,
    BuyButton,
    IconWrapper,
    DepositInputWrapper,
    BackNeon
} from './BuyTicketStyle.js'
import { color2 } from '../../assets/colors.js'
import ColoredForm from '../Forms/ColoredForm.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoins } from '@fortawesome/free-solid-svg-icons'
import { buyTicket } from '../../fetchFunctions.js'

const BuyTicket = ({ scale, setLogIn, userHandler }) => {
    const [n1, setN1] = useState('')
    const [n2, setN2] = useState('')
    const [n3, setN3] = useState('')
    const [n4, setN4] = useState('')
    const [n5, setN5] = useState('')
    const [n6, setN6] = useState('')

    const [deposit, setDeposit] = useState(5)
    const [errorMessage, setErrorMessage] = useState('')

    const [animateLever, setAnimateLever] = useState(false)
    const [animateMessage, setAnimateMessage] = useState(false)

    const validateNumber = (number) => {
        let temp = 0
        number === n1 && temp++
        number === n2 && temp++
        number === n3 && temp++
        number === n4 && temp++
        number === n5 && temp++
        number === n6 && temp++

        if (temp > 0) {
            return false
        } else {
            return number
        }
    }

    const validateScreen = (number) => {
        let newNumber = number
        while (validateNumber(newNumber) === false) {
            newNumber = newNumber + 1
            newNumber > 9 && (newNumber = newNumber - 9)
            validateNumber(newNumber)
        }
        return newNumber
    }

    const valueHandler = (e, setValue, valueState) => {
        let number = String(e.target.value)
        let lastDigit = parseInt(number.substring(number.length - 1))
        lastDigit === 0 && (lastDigit = 1)
        if (lastDigit === valueState) {
            setValue(lastDigit)
        } else {
            let validated = validateScreen(parseInt(lastDigit))
            validated !== lastDigit && animateError()
            setValue(validated)
        }
    }

    const drawNumbers = () => {
        const lotteryNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        let ticket = lotteryNumbers.sort(() => 0.5 - Math.random())
        setTimeout(() => setN1(ticket[0]), 30)
        setTimeout(() => setN2(ticket[1]), 60)
        setTimeout(() => setN3(ticket[2]), 90)
        setTimeout(() => setN4(ticket[3]), 120)
        setTimeout(() => setN5(ticket[4]), 150)
        setTimeout(() => setN6(ticket[5]), 180)
    }

    const leverClick = () => {
        setAnimateLever(true)
        setTimeout(() => setAnimateLever(false), 1000)
        drawNumbers()
    }

    const animateError = () => {
        setAnimateMessage(true)
        setTimeout(() => setAnimateMessage(false), 500)
    }

    const depositHandler = (amount) => {
        if (amount < 5) {
            setDeposit(amount)
            setErrorMessage('Za niska kwota depozytu (minimum 5)')
        } else if (amount > 1000) {
            setDeposit(1000)
            setErrorMessage('Za wysoka kwota depozytu (max 1000)')
        } else {
            setDeposit(amount)
            setErrorMessage('')
        }
    }

    const buyTicketHandler = () => {
        document.cookie === ''
            ? setLogIn(true)
            : buyTicket(n1, n2, n3, n4, n5, n6, deposit, setErrorMessage, userHandler)
    }

    return (
        <ComponentWrapper scale={scale}>
            <Roof>{errorMessage}</Roof>
            <Back>
                <RightSide />
                <Hinge />
                <LeverHead onClick={leverClick} className={animateLever ? 'PullLeverHead' : 'shineColor1'} />
                <Lever className={animateLever ? 'PullLever' : null} />
                <Message className={animateMessage ? 'zoom' : null} />
                <Screen>
                    <ScreenCell>
                        <ScreenInput
                            type="number"
                            min="0"
                            max="9"
                            placeholder="0"
                            value={n1}
                            onChange={(e) => valueHandler(e, setN1, n1)}
                        />
                    </ScreenCell>
                    <ScreenCell>
                        <ScreenInput
                            type="number"
                            min="0"
                            max="9"
                            placeholder="0"
                            value={n2}
                            onChange={(e) => valueHandler(e, setN2, n2)}
                        />
                    </ScreenCell>
                    <ScreenCell>
                        <ScreenInput
                            type="number"
                            min="0"
                            max="9"
                            placeholder="0"
                            value={n3}
                            onChange={(e) => valueHandler(e, setN3, n3)}
                        />
                    </ScreenCell>
                    <ScreenCell>
                        <ScreenInput
                            type="number"
                            min="0"
                            max="9"
                            placeholder="0"
                            value={n4}
                            onChange={(e) => valueHandler(e, setN4, n4)}
                        />
                    </ScreenCell>
                    <ScreenCell>
                        <ScreenInput
                            type="number"
                            min="0"
                            max="9"
                            placeholder="0"
                            value={n5}
                            onChange={(e) => valueHandler(e, setN5, n5)}
                        />
                    </ScreenCell>
                    <ScreenCell noBorder={true}>
                        <ScreenInput
                            type="number"
                            min="0"
                            max="9"
                            placeholder="0"
                            value={n6}
                            onChange={(e) => valueHandler(e, setN6, n6)}
                        />
                    </ScreenCell>
                </Screen>
                <BackNeon />
            </Back>
            <LeftSide />
            <Front>
                <DepositInputWrapper color={color2}>
                    <div>Depozyt:</div>
                    <ColoredForm
                        type="number"
                        name="deposit"
                        color={color2}
                        value={deposit}
                        setValue={depositHandler}
                    />
                </DepositInputWrapper>
                <IconWrapper>
                    <FontAwesomeIcon icon={faCoins} />
                </IconWrapper>
                <BuyButton color={color2} onClick={buyTicketHandler}>
                    Kup los
                </BuyButton>
            </Front>
        </ComponentWrapper>
    )
}

export default BuyTicket
