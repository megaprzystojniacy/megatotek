import React from 'react'
import { Logo, Title } from './WebNameStyle.js'
import logo from '../../assets/logo.png'

const Webname = () => {
    return (
        <Title>
            <Logo src={logo} alt="logo strony" />
            <br />
            megatotek
        </Title>
    )
}

export default Webname
