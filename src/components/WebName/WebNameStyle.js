import styled from 'styled-components'
import { color3 } from '../../assets/colors.js'

export const Title = styled.h1`
    font-size: 2.2rem;
    //text-shadow: 0 0 9px ${color3};
`

export const Logo = styled.img`
    height: 1.5em;
`
