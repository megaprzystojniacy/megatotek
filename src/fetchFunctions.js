export const logIn = (login, password, setErrorCode) => {
    fetch('/api/auth/login', {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            login: login,
            password: password
        })
    })
        .then((response) => response.json())
        .then((json) => {
            setErrorCode(json)
        })
}

export const register = (login, email, password, setErrorCode) => {
    fetch('/api/auth/register', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            login: login,
            password: password,
            email: email
        })
    })
        .then((response) => response.json())
        .then((json) => {
            setErrorCode(json)
        })
}

export const logout = () => {
    fetch('/api/auth/logout', {
        method: 'GET',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((response) => response.json())
}

export const getUserData = (
    limit,
    setUserData,
    setUnusedUserTickets,
    setUsedUserTickets,
    setNumberOfUserTickets
) => {
    fetch('/api/auth/user?limit=' + limit, {
        method: 'GET',
        credentials: 'include',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then((response) => response.json())
        .then((json) => {
            setNumberOfUserTickets(json.result.numberOfUserTickets)
            setUserData(json.result.userData)
            setUnusedUserTickets(json.result.userTickets.unusedUserTickets)
            setUsedUserTickets(json.result.userTickets.usedUserTickets)
        })
}

export const buyTicket = (n1, n2, n3, n4, n5, n6, deposit, setErrorMessage, userHandler) => {
    fetch('/api/lottery/ticket/buy', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify({
            n1: n1,
            n2: n2,
            n3: n3,
            n4: n4,
            n5: n5,
            n6: n6,
            deposit: deposit
        })
    })
        .then((response) => response.json())
        .then((json) => {
            userHandler()
            setErrorMessage(json.result)
        })
}

export const getWinningTickets = (setWinningTickets, setWinningTicket) => {
    fetch('/api/lottery/tickets', {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then((response) => response.json())
        .then((json) => {
            setWinningTicket(json.result[0])
            json.result.shift()
            setWinningTickets(json.result)
        })
}

export const addFunds = (funds, userHandler) => {
    fetch('/api/wallet/funds/add', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify({
            funds: funds
        })
    })
        .then((response) => response.json())
        .then(() => {
            userHandler()
        })
}
