import mongoose from 'mongoose'

const ticketSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    n1: {
        type: Number,
        required: true
    },
    n2: {
        type: Number,
        required: true
    },
    n3: {
        type: Number,
        required: true
    },
    n4: {
        type: Number,
        required: true
    },
    n5: {
        type: Number,
        required: true
    },
    n6: {
        type: Number,
        required: true
    },
    deposit: {
        type: Number,
        required: true
    },
    used: {
        type: Boolean,
        required: true,
        default: false
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now()
    },
    updatedAt: {
        type: Date,
        default: () => Date.now()
    }
})

export const Ticket = mongoose.model('Ticket', ticketSchema)
