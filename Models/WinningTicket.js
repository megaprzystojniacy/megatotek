import mongoose from 'mongoose'

const winningTicketSchema = new mongoose.Schema({
    n1: {
        type: Number,
        required: true
    },
    n2: {
        type: Number,
        required: true
    },
    n3: {
        type: Number,
        required: true
    },
    n4: {
        type: Number,
        required: true
    },
    n5: {
        type: Number,
        required: true
    },
    n6: {
        type: Number,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        default: null
    },
    highestPrize: {
        type: Number,
        default: 0
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now()
    },
    updatedAt: {
        type: Date,
        default: () => Date.now()
    }
})

export class TicketClass {
    constructor(highestPrize) {
        this.highestPrize = highestPrize
    }

    setPrize(prize) {
        this.highestPrize = prize
    }

    getPrize() {
        return this.highestPrize
    }

    setUserId(userId) {
        this.userId = userId
    }

    getUserId() {
        return this.userId
    }

    setTicketId(ticketId) {
        this.ticketId = ticketId
    }

    getTicketId() {
        return this.ticketId
    }

    clearData() {
        this.highestPrize = 0
        this.userId = null
        this.ticketId = null
    }
}

export const WinningTicket = mongoose.model('WinningTicket', winningTicketSchema)
