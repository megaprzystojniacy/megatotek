import mongoose from 'mongoose'

const userSchema = new mongoose.Schema({
    login: {
        type: String,
        min: 3,
        required: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        min: 6,
        required: true
    },
    balance: {
        type: Number,
        default: 100
    },
    type: {
        type: Number,
        default: 1
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now()
    },
    updatedAt: {
        type: Date,
        default: () => Date.now()
    }
})

export const User = mongoose.model('User', userSchema)
