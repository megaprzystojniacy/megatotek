import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import cors from 'cors'
import { serverRoutes } from './Routes/Routes.js'
import { databaseConnection } from './DatabaseConnection.js'
import dotenv from 'dotenv'
import { lotteryCronJob } from './Routes/Lottery/LotteryFunctions.js'
import path from 'path'
import { fileURLToPath } from 'url'
import morgan from 'morgan'

const app = express()
const port = process.env.PORT || 3000
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

/**
 * Server configuration
 */
app.use(
    morgan('common'),
    express.json(),
    cookieParser(),
    bodyParser.urlencoded({ extended: true }),
    cors({
        credentials: true,
        origin: (_, callback) => {
            callback(null, true)
        }
    }),
    express.static(path.join(__dirname + '/build')),
    serverRoutes
)

dotenv.config()

databaseConnection()
process.env.MT_ENV === 'production' && lotteryCronJob()

app.listen(port, () => {
    console.log(`Server started!`)
})
