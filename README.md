# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

## Description
MegaTotek is an internet lottery web application. <br>
It is based on an economy of MegaTotek coins which you can spend on buying lottery tickets and win more coins from the lottery itself. <br>
It was inspired by polish Totolotek. <br>


## URL
The URL to our website: https://megatotek.onrender.com


## Usage
Website is divided into 3 panels:
- left panel with your lottery tickets history
- central panel where you can buy more tickets
- right panel where you can log into your account and manage your accounts balance

**To buy a new lottery ticket you need to:**
1. Log into your account
2. Write yourself or draw a random lottery ticket
3. Write the amount of coins you want to pay for the ticket
4. Buy a ticket
5. Wait for the lottery!

Every user can buy **as many tickets as he wants** (as long as he has credit for them).

The lottery will happen every 5 minutes.


## Authors
[] Katarzyna Pstrokońska <br>
[] Bartosz Poszelężny <br>
[] Karol Niemczuk <br>
<br>

### Project status
*In development*
