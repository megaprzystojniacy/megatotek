/**
 *  Welcome mail content
 */
export const generateWelcomeEmail = (login) => {
    return {
        subject: 'Witamy w Megatotek!',
        html:
            `<h1>Witaj ${login} w serwisie Megatotek!</h1>` +
            '<p>Skorzystaj z poniższego linku, aby doładować konto i cieszczyć się z <b>WIELKICH WYGRANYCH</b>!</p> ' +
            '<a href="https://megatotek.onrender.com">megatotek.pl</a>'
    }
}

/**
 *  New draw notification mail content
 */
export const generateNewDrawNotificationEmail = (date, numbers, highestPrize) => {
    return {
        subject: `Wyniki losowania z ${date}!`,
        html:
            `<h1>Zwycięskie liczby z ${date}</h1>` +
            `<h2>${numbers.n1} ${numbers.n2} ${numbers.n3} ${numbers.n4} ${numbers.n5} ${numbers.n6}</h2>` +
            `<p>Najwyższa wygrana: <b>${highestPrize}</b>$!</p> ` +
            '<a href="https://megatotek.onrender.com">megatotek.pl</a>'
    }
}
