import { createTransport } from 'nodemailer'

export const sendEmail = (email, content) => {
    const transporter = createTransport({
        port: 465,
        host: 'smtp.poczta.onet.pl',
        auth: {
            user: process.env.EMAIL,
            pass: process.env.EMAIL_PASSWORD
        },
        secure: true
    })

    const mailData = {
        from: process.env.EMAIL,
        to: email,
        subject: content.subject,
        html: content.html
    }

    transporter.sendMail(mailData, (err, info) => {
        if (err) console.log(err)
        else console.log(info)
    })
}

export class TicketEmailClass {
    constructor() {
        this.userId = []
        this.numbers = {
            n1: 0,
            n2: 0,
            n3: 0,
            n4: 0,
            n5: 0,
            n6: 0
        }
        this.highestPrize = 0
    }

    addUserId(userId) {
        if (!this.userId.includes(userId)) {
            this.userId.push(userId)
        }
    }

    getUserId() {
        return this.userId
    }

    getDate() {
        return (
            new Date().getHours() + ':' + (new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes()
        )
    }

    setNumbers(numbers) {
        this.numbers = {
            n1: numbers.n1,
            n2: numbers.n2,
            n3: numbers.n3,
            n4: numbers.n4,
            n5: numbers.n5,
            n6: numbers.n6
        }
    }

    getNumbers() {
        return this.numbers
    }

    setHighestPrize(highestPrize) {
        this.highestPrize = highestPrize
    }

    getHighestPrize() {
        return this.highestPrize
    }

    clearData() {
        this.userId = []
        this.numbers = {
            n1: 0,
            n2: 0,
            n3: 0,
            n4: 0,
            n5: 0,
            n6: 0
        }
        this.highestPrize = 0
    }
}
