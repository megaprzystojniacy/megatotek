import mongoose from 'mongoose'
import dotenv from 'dotenv'
dotenv.config()

// Create connection
export const databaseConnection = () => {
    mongoose.connect(process.env.URI, () => {
        console.log('Database connected!')
    })
}
