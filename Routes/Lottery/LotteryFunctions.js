import { Ticket } from '../../Models/Ticket.js'
import { databaseError } from '../../Library/Errors.js'
import { drawInterval, lotteryNumbers, prizeMultiplier } from '../../Library/DataTemplates.js'
import { User } from '../../Models/User.js'
import cron from 'node-cron'
import { TicketClass, WinningTicket } from '../../Models/WinningTicket.js'
import { sendEmail, TicketEmailClass } from '../../Emailer/Mailer.js'
import { generateNewDrawNotificationEmail } from '../../Emailer/EmailContent.js'
import { getUserData } from '../Auth/UserData/UserDataFunctions.js'

let winningTicket = new TicketClass(0)
let emailData = new TicketEmailClass()

export const lotteryCronJob = () => {
    cron.schedule(drawInterval, () => {
        drawWinningTicket().then(() =>
            settleTheDraw().then(() => {
                winningTicket.clearData()
                emailData.getUserId().forEach((userId) => {
                    getUserData(userId).then((userData) => {
                        sendNewDrawNotificationEmail(userData.email)
                    })
                })
            })
        )
    })
}

// TODO... remove this after tests
export const temp = () => {
    drawWinningTicket().then(() =>
        settleTheDraw().then(() => {
            winningTicket.clearData()
            emailData.getUserId().forEach((userId) => {
                getUserData(userId).then((userData) => {
                    sendNewDrawNotificationEmail(userData.email)
                })
            })
        })
    )
}

export const drawWinningTicket = async () => {
    let winningTicket = lotteryNumbers.sort(() => 0.5 - Math.random()).slice(0, 6)
    try {
        await WinningTicket.create({
            n1: winningTicket[0],
            n2: winningTicket[1],
            n3: winningTicket[2],
            n4: winningTicket[3],
            n5: winningTicket[4],
            n6: winningTicket[5]
        })
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

export const settleTheDraw = async () => {
    emailData.clearData()
    let finished = new Promise((resolve) => {
        getUnusedTickets().then((unusedTickets) => {
            useTickets()
            getLatestWinningTicket().then((latestWinningTicket) => {
                emailData.setNumbers(latestWinningTicket)
                unusedTickets.forEach((unusedTicket) => {
                    let currentMatches = countMatches(unusedTicket, latestWinningTicket)
                    if (currentMatches >= 2) {
                        payOutThePrize(unusedTicket.userId, unusedTicket.deposit, currentMatches)
                    }
                    emailData.addUserId(unusedTicket.userId)
                })
                saveHighestWin()
                resolve('true')
            })
        })
    })
    return await finished
}

export const getUnusedTickets = async () => {
    try {
        let unusedTickets = await Ticket.find({ used: false })
        return unusedTickets
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const getLatestWinningTicket = async () => {
    try {
        let result = await WinningTicket.find().sort({ createdAt: 'desc' }).limit(1)
        winningTicket.setTicketId(result[0].id)
        return {
            n1: result[0].n1,
            n2: result[0].n2,
            n3: result[0].n3,
            n4: result[0].n4,
            n5: result[0].n5,
            n6: result[0].n6
        }
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

export const useTickets = async () => {
    try {
        await Ticket.updateMany({ used: true })
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const countMatches = (ticket, latestWinningTicket) => {
    let latestWinningTicketArray = objectToArray(latestWinningTicket)
    let matches = 0
    let currentTicket = {
        n1: ticket.n1,
        n2: ticket.n2,
        n3: ticket.n3,
        n4: ticket.n4,
        n5: ticket.n5,
        n6: ticket.n6
    }
    Object.values(currentTicket).forEach((ticketNumber, index) => {
        ticketNumber === latestWinningTicketArray[index] && matches++
    })
    return matches
}

const payOutThePrize = (userId, deposit, matches) => {
    let prize = deposit * prizeMultiplier[matches - 1]
    if (prize > winningTicket.getPrize()) {
        winningTicket.setPrize(prize)
        emailData.setHighestPrize(prize)
        winningTicket.setUserId(userId)
    }
    User.findByIdAndUpdate(userId, { $inc: { balance: prize } }, (e) => {
        if (e)
            throw new databaseError({
                errorName: e.name,
                errorMessage: e.message
            })
    })
}

export const saveHighestWin = async () => {
    try {
        await WinningTicket.findByIdAndUpdate(winningTicket.getTicketId(), {
            user: winningTicket.getUserId(),
            highestPrize: winningTicket.getPrize()
        })
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const sendNewDrawNotificationEmail = (email) => {
    let emailContent = generateNewDrawNotificationEmail(
        emailData.getDate(),
        emailData.getNumbers(),
        emailData.getHighestPrize()
    )
    sendEmail(email, emailContent)
}

const objectToArray = (object) => {
    let result = []
    Object.values(object).forEach((value) => {
        result.push(value)
    })
    return result
}
