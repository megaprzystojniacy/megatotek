import express from 'express'
import { authenticate, getJwtData } from '../../Auth/Login/LoginFunctions.js'
import { buyTicket, getWinningTickets } from './TicketFunctions.js'
import { temp } from '../LotteryFunctions.js'

const router = express.Router()

export const buyTicketRoute = router.post('/api/lottery/ticket/buy', authenticate, (req, res) => {
    let jwtData = getJwtData(req.cookies.JWT)
    buyTicket(
        jwtData.id,
        req.body.n1,
        req.body.n2,
        req.body.n3,
        req.body.n4,
        req.body.n5,
        req.body.n6,
        req.body.deposit,
        res
    )
})

export const getWinningTicketsRoute = router.get('/api/lottery/tickets', (req, res) => {
    getWinningTickets()
        .then((winningTickets) => {
            res.json({ result: winningTickets })
        })
        .catch((e) => {
            res.json({ result: e.message, type: e.type })
        })
})

export const tempRoute = router.get('/temp', (req, res) => {
    temp()
    res.send('wylosowano')
})
