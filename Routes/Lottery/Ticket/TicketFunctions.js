import { badDataError, databaseError, lowAccountBalance, tooLowDeposit } from '../../../Library/Errors.js'
import { User } from '../../../Models/User.js'
import { minTicketCost } from '../../../Library/DataTemplates.js'
import { Ticket } from '../../../Models/Ticket.js'
import { WinningTicket } from '../../../Models/WinningTicket.js'
import { newTicketCallback } from '../../../Library/Callbacks.js'

export const buyTicket = (userId, n1, n2, n3, n4, n5, n6, deposit, res) => {
    if (userId && n1 && n2 && n3 && n4 && n5 && n6 && deposit) {
        if (deposit < minTicketCost) {
            res.json(tooLowDeposit)
        } else {
            checkAccountBalance(userId)
                .then((accountBalance) => {
                    if (accountBalance < deposit) {
                        res.json(lowAccountBalance)
                    } else {
                        ticketCharge(userId, deposit)
                            .then(() => addNewTicket(userId, n1, n2, n3, n4, n5, n6, deposit))
                            .catch((e) => {
                                res.json({ result: e.message, type: e.type })
                            })
                        res.json(newTicketCallback)
                    }
                })
                .catch((e) => {
                    res.json({ result: e.message, type: e.type })
                })
        }
    } else {
        res.json(badDataError)
    }
}

const checkAccountBalance = async (userId) => {
    try {
        let result = await User.findById(userId)
        return result.balance
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const addNewTicket = async (userId, n1, n2, n3, n4, n5, n6, deposit) => {
    try {
        await Ticket.create({
            userId: userId,
            n1: n1,
            n2: n2,
            n3: n3,
            n4: n4,
            n5: n5,
            n6: n6,
            deposit: deposit
        })
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const ticketCharge = async (userId, deposit) => {
    try {
        await User.findByIdAndUpdate(userId, { $inc: { balance: -deposit } })
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

export const getWinningTickets = async () => {
    try {
        let winningTickets = await WinningTicket.find()
            .populate('user', 'login')
            .select({ updatedAt: 0, __v: 0 })
            .sort({ createdAt: 'desc' })
            .limit(10)
        return winningTickets
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}
