import express from 'express'
import { authenticate, getJwtData } from '../Auth/Login/LoginFunctions.js'
import { addFunds, withdrawFunds } from './WalletFunctions.js'

const router = express.Router()

export const addFundsRoute = router.post('/api/wallet/funds/add', authenticate, (req, res) => {
    let jwtData = getJwtData(req.cookies.JWT)
    addFunds(jwtData.id, req.body.funds, res)
})

export const withdrawFundsRoute = router.post('/api/wallet/funds/withdraw', authenticate, (req, res) => {
    let jwtData = getJwtData(req.cookies.JWT)
    withdrawFunds(jwtData.id, req.body.funds, res)
})
