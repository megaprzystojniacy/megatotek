import { User } from '../../Models/User.js'
import { addFundsCallback, withdrawFundsCallback } from '../../Library/Callbacks.js'
import { databaseError, fundsValidationError } from '../../Library/Errors.js'

export const addFunds = (id, funds, res) => {
    fundsHandler(id, funds, 'add')
        .then((result) => {
            res.json(result)
        })
        .catch((e) => {
            res.json({ result: e.message, type: e.type })
        })
}

export const withdrawFunds = (id, funds, res) => {
    fundsHandler(id, funds, 'withdraw')
        .then((result) => {
            res.json(result)
        })
        .catch((e) => {
            res.json({ result: e.message, type: e.type })
        })
}

const fundsValidation = (funds) => {
    return parseInt(funds) > 0
}

const fundsHandler = async (id, funds, action) => {
    if (fundsValidation(funds)) {
        try {
            switch (action) {
                case 'add':
                    await User.findByIdAndUpdate(id, { $inc: { balance: funds } })
                    return addFundsCallback
                case 'withdraw':
                    await User.findByIdAndUpdate(id, { $inc: { balance: -funds } })
                    return withdrawFundsCallback
            }
        } catch (e) {
            throw new databaseError({
                errorName: e.name,
                errorMessage: e.message
            })
        }
    } else {
        return fundsValidationError
    }
}
