import { User } from '../../../Models/User.js'
import { databaseError } from '../../../Library/Errors.js'
import { Ticket } from '../../../Models/Ticket.js'

export const userData = (userId, limit, res) => {
    getUserData(userId)
        .then((userData) => {
            getUserTickets(userId, limit)
                .then((userTickets) => {
                    countUserTickets(userId)
                        .then((numberOfUserTickets) => {
                            res.json({
                                result: {
                                    userData: userData,
                                    userTickets: userTickets,
                                    numberOfUserTickets: numberOfUserTickets
                                }
                            })
                        })
                        .catch((e) => {
                            res.json({ result: e.message, type: e.type })
                        })
                })
                .catch((e) => {
                    res.json({ result: e.message, type: e.type })
                })
        })
        .catch((e) => {
            res.json({ result: e.message, type: e.type })
        })
}

export const getUserData = async (userId) => {
    try {
        let userData = await User.findById(userId).select({ password: 0, __v: 0, createdAt: 0, updatedAt: 0 })
        return userData
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const getUserTickets = async (userId, limit) => {
    let userTickets = new Promise((resolve) => {
        getUnusedUserTickets(userId).then((unusedUserTickets) => {
            getUsedUserTickets(userId, limit).then((usedUserTickets) => {
                resolve({
                    unusedUserTickets: unusedUserTickets,
                    usedUserTickets: usedUserTickets
                })
            })
        })
    })
    return await userTickets
}

const getUnusedUserTickets = async (userId) => {
    try {
        let unusedUserTickets = Ticket.find()
            .where({ userId: userId, used: false })
            .select({ userId: 0, __v: 0, updatedAt: 0 })
            .sort({ createdAt: 'desc' })
        return unusedUserTickets
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const getUsedUserTickets = async (userId, limit) => {
    try {
        let usedUserTickets = Ticket.find()
            .where({ userId: userId, used: true })
            .select({ userId: 0, __v: 0, updatedAt: 0 })
            .sort({ createdAt: 'desc' })
            .limit(limit)
        return usedUserTickets
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const countUserTickets = async (userId) => {
    try {
        let numberOfUserTickets = Ticket.where({ userId: userId }).count()
        return numberOfUserTickets
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}
