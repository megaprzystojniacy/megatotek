import express from 'express'
import { authenticate, getJwtData } from '../Login/LoginFunctions.js'
import { userData } from './UserDataFunctions.js'
const router = express.Router()

export const userDataRoute = router.get('/api/auth/user', authenticate, (req, res) => {
    let jwtData = getJwtData(req.cookies.JWT)
    userData(jwtData.id, req.query.limit, res)
})
