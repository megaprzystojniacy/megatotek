import { authTemplate } from '../../../Library/DataTemplates.js'
import { badDataError, databaseError, userLoginError } from '../../../Library/Errors.js'
import { User } from '../../../Models/User.js'
import bcrypt from 'bcrypt'
import { userLoginCallback, logoutCallback } from '../../../Library/Callbacks.js'
import jwt from 'jsonwebtoken'

export const login = (login, password, res) => {
    if (login && password) {
        if (loginDataValidation(login, password)) {
            res.json(userLoginError)
        } else {
            getUserData(login)
                .then((userData) => {
                    if (userData !== 0) {
                        userPasswordValidation(password, userData.password, (passwordValidation) => {
                            if (passwordValidation) {
                                let accessToken = generateAccessToken(
                                    userData.id,
                                    userData.login,
                                    userData.type
                                )
                                res.cookie('JWT', accessToken, {
                                    maxAge: 36000000,
                                    httpOnly: false,
                                    sameSite: 'none',
                                    secure: true //TODO... just for tests
                                })
                                res.json(userLoginCallback)
                            } else {
                                res.json(userLoginError)
                            }
                        })
                    } else {
                        res.json(userLoginError)
                    }
                })
                .catch((e) => {
                    res.json({ result: e.message, type: e.type })
                })
        }
    } else {
        res.json(badDataError)
    }
}

const loginDataValidation = (login, password) => {
    return !(login.length >= authTemplate.login && password.length >= authTemplate.password)
}

const getUserData = async (login) => {
    try {
        let data = await User.findOne({ login: login })
        if (data) {
            return data
        } else {
            return 0
        }
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const userPasswordValidation = (decryptedPassword, hashedPassword, callback) => {
    bcrypt.compare(decryptedPassword, hashedPassword, (err, login) => {
        if (login) {
            return callback(1)
        } else {
            return callback(0)
        }
    })
}

const generateAccessToken = (id, login, type) => {
    return jwt.sign(
        {
            id: id,
            login: login,
            type: type
        },
        process.env.TOKEN_SECRET,
        { expiresIn: 36000000 }
    )
}

export const authenticate = (req, res, next) => {
    const token = req.cookies.JWT

    if (token === null) return res.sendStatus(401)

    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403)

        req.user = user
        next()
    })
}

export const getJwtData = (token, res) => {
    let results
    if (token) {
        jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
            if (err) res.sendStatus(400)

            results = decoded
        })
        return results
    } else {
        return null
    }
}

export const logout = (res) => {
    res.clearCookie('JWT')
    res.json(logoutCallback)
}
