import express from 'express'
import { authenticate, login, logout } from './LoginFunctions.js'

const router = express.Router()

export const loginRoute = router.post('/api/auth/login', (req, res) => {
    login(req.body.login, req.body.password, res)
})

export const logoutRoute = router.get('/api/auth/logout', authenticate, (req, res) => {
    logout(res)
})
