import { authTemplate } from '../../../Library/DataTemplates.js'
import bcrypt from 'bcrypt'
import { badDataError, databaseError, userExistsError } from '../../../Library/Errors.js'
import { userRegistrationCallback } from '../../../Library/Callbacks.js'
import { User } from '../../../Models/User.js'
import { sendEmail } from '../../../Emailer/Mailer.js'
import { generateWelcomeEmail } from '../../../Emailer/EmailContent.js'

export const registration = (login, email, password, res) => {
    if (login && email && password) {
        if (registrationValidation(login, email, password)) {
            res.json(badDataError)
        } else {
            doesUserExists(login)
                .then((doesUserExistsBool) => {
                    if (doesUserExistsBool) {
                        res.json(userExistsError)
                    } else {
                        generateHashedPassword(password).then((hashedPassword) => {
                            registerUser(login, email, hashedPassword).then((registrationCallback) => {
                                sendEmail(email, generateWelcomeEmail(login))
                                res.json(registrationCallback)
                            })
                        })
                    }
                })
                .catch((e) => {
                    res.json({ result: e.message, type: e.type })
                })
        }
    } else {
        res.json(badDataError)
    }
}

const registrationValidation = (login, email, password) => {
    return !(
        login.length >= authTemplate.login &&
        email.includes(authTemplate.email) &&
        password.length >= authTemplate.password
    )
}

const doesUserExists = async (login) => {
    try {
        let result = await User.exists({ login: login }).count()
        return result
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}

const generateHashedPassword = async (password, rounds) => {
    const salt = await bcrypt.genSalt(rounds)
    return bcrypt.hash(password, salt)
}

const registerUser = async (login, email, hashedPassword) => {
    try {
        await User.create({ login: login, email: email, password: hashedPassword })
        return userRegistrationCallback
    } catch (e) {
        throw new databaseError({
            errorName: e.name,
            errorMessage: e.message
        })
    }
}
