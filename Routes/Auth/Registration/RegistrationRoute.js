import express from 'express'
import { registration } from './RegistrationFunctions.js'

const router = express.Router()

export const registrationRoute = router.post('/api/auth/register', (req, res) => {
    registration(req.body.login, req.body.email, req.body.password, res)
})
