import express from 'express'
import { registrationRoute } from './Auth/Registration/RegistrationRoute.js'
import { loginRoute, logoutRoute } from './Auth/Login/LoginRoute.js'
import { buyTicketRoute, getWinningTicketsRoute } from './Lottery/Ticket/TicketRoute.js'
import { userDataRoute } from './Auth/UserData/UserDataRoute.js'
import {addFundsRoute, withdrawFundsRoute} from './Wallet/WalletRoute.js'

const app = express()

export const serverRoutes = app.use(
    registrationRoute,
    loginRoute,
    logoutRoute,
    buyTicketRoute,
    getWinningTicketsRoute,
    userDataRoute,
    addFundsRoute,
    withdrawFundsRoute
)
